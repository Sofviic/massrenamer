# MassRenamer

A batch file renamer in C#

# Usage

Was used to trim leading digits and underscores from files in a directory. <br>
Set `dir` in line 13 to the directory containg all files to be renamed. <br>
Set `charsToRemove` in line 17 to the string containing all the characters to be trimmed from the start of file names. <br>
<br>
Note: This was only tested on windows, not on linux.
For Linux just use `mv` anyway.

## Roadmap

Might improve it in the future. But for now it has done its task for me.

## License

```
Copyright (C) 2023 sof

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

