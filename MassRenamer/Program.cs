﻿
using System.Text.RegularExpressions;

public static class Program {
	public static bool RenameFile(string filepath, Func<char, bool> undesirables, string skipRegex, int minNumOfCharsInFilename = 5) {
		string directory = Path.GetDirectoryName(filepath)!;
		string filename = Path.GetFileName(filepath);
		if(Regex.Match(filename, skipRegex).Success) {
			Console.WriteLine($"Skipping file {filename}...");
			return true;
		}
		string trimmedName = new(filename.SkipWhile(undesirables).ToArray());
		if(trimmedName.Length < minNumOfCharsInFilename) trimmedName = new(filename.Reverse().Take(minNumOfCharsInFilename).Reverse().ToArray());
		Console.Write($"Renaming file {filename} to {trimmedName}...");
		File.Move(filepath, Path.Combine(directory, trimmedName), true);
		Console.WriteLine($"done");
		return true;
	}

	public static bool RenameFile(string filepath, int trimCount, string skipRegex, int minNumOfCharsInFilename = 5) {
		string directory = Path.GetDirectoryName(filepath)!;
		string filename = Path.GetFileName(filepath);
		if(Regex.Match(filename, skipRegex).Success) {
			Console.WriteLine($"Skipping file {filename}...");
			return true;
		}
		string trimmedName = new(filename.Skip(trimCount).ToArray());
		if(trimmedName.Length < minNumOfCharsInFilename) trimmedName = new(filename.Reverse().Take(minNumOfCharsInFilename).Reverse().ToArray());
		Console.Write($"Renaming file {filename} to {trimmedName}...");
		File.Move(filepath, Path.Combine(directory, trimmedName), true);
		Console.WriteLine($"done");
		return true;
	}

	public static void Main(string[] args) {
		string dir = @"<Insert Path To Directory Here>"; // <- Insert Path To Directory Here
		string[] files = Directory.EnumerateFiles(dir, "*", SearchOption.TopDirectoryOnly).ToArray();
		Console.WriteLine($"Renaming {files.Count()} files in directory {dir}");

		//foreach(string f in files) RenameFile(f, 5, @"^(\w|null).png$");

		//files = Directory.EnumerateFiles(dir, "*", SearchOption.TopDirectoryOnly).ToArray(); // <- between calls recompute file names

		string charsToRemove = "_0123456789"; // <- Insert Leading Characters to Trim Here
		foreach(string f in files) RenameFile(f, charsToRemove.Contains, @"^(\w|null).png$");

		Console.ReadLine();
	}
}
